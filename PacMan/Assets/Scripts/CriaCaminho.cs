﻿using UnityEngine;
using System.Collections;

public enum DIRECAO{
    RIGHT,
    DOWN,
    LEFT,
    UP
}

public class CriaCaminho {
    public static PriorityQueue closedList, openList;

    
    private static float HeuristicEstimateCost(WaypointHandler curNode, WaypointHandler goalNode)
    {
        Vector3 vecCost = curNode.position - goalNode.position;
        return vecCost.magnitude;
    }

    public static ArrayList menorCaminhoEntre(Transform start, Transform goal)
    {
        return menorCaminhoEntre(start.GetComponent<WaypointHandler>(), goal.GetComponent<WaypointHandler>());
    }

    public static ArrayList menorCaminhoEntre(Transform start, WaypointHandler goal)
    {
        return menorCaminhoEntre(start.GetComponent<WaypointHandler>(), goal);
    }

    //encontra o menor caminho
    public static ArrayList menorCaminhoEntre(WaypointHandler start, WaypointHandler goal)
    {
        ArrayList returnPath = new ArrayList();
        WaypointHandler[] grid = GridManager.instance.Waypoints;       
        openList =  new PriorityQueue();

        
        if(start.name==goal.name)
        {
            returnPath.Add(goal);
            return returnPath;
        }
        foreach(Object node in grid)
        {
            ((WaypointHandler)node).estimatedCost = CriaCaminho.HeuristicEstimateCost((WaypointHandler)node, goal);
            openList.Push((WaypointHandler)node);          
        }

        
        WaypointHandler atual = GridManager.getWayPointByName(start.name);


        WaypointHandler nextWaypoint = atual.NextRight != null && openList.Contains(atual.NextRight) ? atual.NextRight :
                                        (atual.NextDown != null && openList.Contains(atual.NextDown) ? atual.NextDown :
                                            (atual.NextLeft != null && openList.Contains(atual.NextLeft) ? atual.NextLeft : atual.NextUP)
                                         );
        returnPath.Add(start);

        int i = 100;
        while (atual != goal && openList.Length>1 && i-->0)
        {
            if (atual.NextDown!=null)
                if (openList.Contains(atual.NextDown) && nextWaypoint.CompareTo(atual.NextDown) > 0)
                {                                      
                    nextWaypoint = atual.NextDown;
                }

            if (atual.NextLeft != null)
                if (openList.Contains(atual.NextLeft) && nextWaypoint.CompareTo(atual.NextLeft) > 0)
                {
                   
                    nextWaypoint = atual.NextLeft;
                }

            if (atual.NextUP != null)
                if (openList.Contains(atual.NextUP) && nextWaypoint.CompareTo(atual.NextUP) > 0)
                {
                                      
                    nextWaypoint = atual.NextUP;
                }

            if (atual.NextRight != null)
                if (openList.Contains(atual.NextRight) && nextWaypoint.CompareTo(atual.NextRight) > 0)
                {                                     
                    nextWaypoint = atual.NextRight;
                }

            openList.Remove(nextWaypoint); 

            if (!returnPath.Contains(nextWaypoint))
            {
                returnPath.Add(nextWaypoint);
                atual = nextWaypoint;
            }

            nextWaypoint = atual.NextRight != null && openList.Contains(atual.NextRight) ? atual.NextRight :
                                        (atual.NextDown != null && openList.Contains(atual.NextDown) ? atual.NextDown :
                                            (atual.NextLeft != null && openList.Contains(atual.NextLeft) ? atual.NextLeft : atual.NextUP)
                                         );
            
        };


        //return cleanPath(returnPath);
        return returnPath;
    }


    public static ArrayList wanderBetween(Transform start, Transform goal)
    {
        return wanderBetween(start.GetComponent<WaypointHandler>(), goal.GetComponent<WaypointHandler>());
    }

    public static ArrayList wanderBetween(Transform start, WaypointHandler goal)
    {
        return wanderBetween(start.GetComponent<WaypointHandler>(), goal);
    }

    public static ArrayList wanderBetween(WaypointHandler start, WaypointHandler goal)
    {
        ArrayList returnPath = new ArrayList();

        
        ArrayList branch1 = menorCaminhoEntre(start, GridManager.getRandomWaypoint());
        ArrayList branch2 = menorCaminhoEntre((WaypointHandler)(branch1[branch1.Count-1]), GridManager.getRandomWaypoint());
        ArrayList branch3 = menorCaminhoEntre((WaypointHandler)(branch2[branch2.Count-1]), goal);

        returnPath.AddRange(branch1);
        returnPath.AddRange(branch2);
        returnPath.AddRange(branch3);  
       
       return returnPath;

    }


    //remove loops no caminho
    public static ArrayList cleanPath(ArrayList waypoints)
    {
        bool canLeave = false;
        bool iNeedToBreak = false;
        int controle = waypoints.Count; //so para garantir que nao haja nenhum loop. Talvez seja computação desnecessaria :P
        do
        {
            for (int i = 0; i < waypoints.Count; i++)
            {
                for (int j = i+1; j < waypoints.Count; j++)
                {
                    if (((WaypointHandler)waypoints[i]).name == ((WaypointHandler)waypoints[j]).name)
                    {
                        //Loop encontrado
                        if (j == waypoints.Count-1)
                            canLeave = true;
                        else
                            iNeedToBreak = true;

                        waypoints.RemoveRange(i, j - i);                        
                        break;
                    }

                }
                if (iNeedToBreak)
                    break;
            }
            controle--;
        } while (!canLeave && controle > 0);

        return waypoints;
    }
	
    public static ArrayList continuaCaminho(WaypointHandler start, WaypointHandler next)
    {
        ArrayList returnPath = new ArrayList();


        return returnPath;
    }


   
}
