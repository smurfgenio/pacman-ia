﻿using UnityEngine;
using System.Collections;

public class PriorityQueue
{
    private ArrayList nodes = new ArrayList();
    public int Length
    {
        get { return this.nodes.Count; }
    }
    public bool Contains(object node)
    {
        return this.nodes.Contains(node);
    }
    public WaypointHandler First()
    {
        if (this.nodes.Count > 0)
        {
            return (WaypointHandler)this.nodes[0];
        }
        return null;
    }

    public void Push(WaypointHandler node)
    {
        this.nodes.Add(node);
        this.nodes.Sort();
    }

    public void Push(ArrayList nodes)
    {
        foreach (WaypointHandler w in nodes)
            Push(w);
    }

    public void Remove(WaypointHandler node)
    {
        this.nodes.Remove(node);
        //Ensure the list is sorted
        this.nodes.Sort();
    }

    public void Clear()
    {
        nodes.Clear();
    }

    

}
