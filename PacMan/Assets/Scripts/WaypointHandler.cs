﻿using UnityEngine;
using System.Collections;
using System;

public class WaypointHandler : MonoBehaviour, IComparable {

    public Transform nextUP;
    public Transform nextDown;
    public Transform nextLeft;
    public Transform nextRight;

    public Transform isPortalto;


    public float nodeTotalCost;
    public float estimatedCost;

    public WaypointHandler()
    {
        this.estimatedCost = 0.0f;
        this.nodeTotalCost = 1.0f;
      
    }

    public Vector3 position {
        get { return gameObject.transform.position; }

    }

    public WaypointHandler NextUP
    {
        get
        {
            if (nextUP != null)
                return nextUP.GetComponent<WaypointHandler>();
            else
                return null;
            
        }
    }

    public WaypointHandler NextRight
    {
        get
        {
            if (nextRight!= null)
                return nextRight.GetComponent<WaypointHandler>();
            else
                return null;

        }
    }

    public WaypointHandler NextDown
    {
        get
        {
            if (nextDown != null)
                return nextDown.GetComponent<WaypointHandler>();
            else
                return null;

        }
    }

    public WaypointHandler NextLeft
    {
        get
        {
            if (nextLeft != null)
                return nextLeft.GetComponent<WaypointHandler>();
            else
                return null;

        }
    }


    //funcao para comparar os pesos dos waypoints
    public int CompareTo(object obj) {
        if (obj == null)
            return 0;

        WaypointHandler node = (WaypointHandler)obj;
        
        //Negative value means object comes before this in the sort
        //order.
        if (this.estimatedCost < node.estimatedCost)
            return -1;
        
        //Positive value means object comes after this in the sort
        //order.
        if (this.estimatedCost > node.estimatedCost) 
            return 1;

        return 0;//default value
      }
        

    
}
