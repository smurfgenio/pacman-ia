﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using UnityEngine;



[RequireComponent(typeof(Rigidbody2D))]
public class Ghost : MonoBehaviour
{
    public GameObject player;
    private MovePacman PlayerScript; 
    public Transform[] ExitPath;
    public Transform[] WaypointsOfInterest; //Pontos que podem guiar a criação do Path, ao inves de usar Random points
    public bool HasMemory = true; //Lembra do ultimo path antes para o Following path. Caso contrario, Ghost cria um novo Path

    [HideInInspector]
    public bool canChasePacman = false;
    [HideInInspector]
    public bool sawPacman = false;

    ArrayList statesToWarnAbout;

    ArrayList exitPath;

    public ESTRATEGIA minhaEstrategia;

    public ESTRATEGIA MinhaEstrategia
    {
        get { return minhaEstrategia; }
        set
        {

            minhaEstrategia = value;
        }
    }

    ArrayList myPath;
    ArrayList currentPath;

    private FSMSystem fsm;
    public Animator myAnim;
    Vector2 newDir;
    public float speed = 5.0f;
    public float rangeOfView = 2.5f;

    [HideInInspector]
    public float originalRangeOfView;

    bool showRange = false;

    public float RangeOfView
    {
        get { return rangeOfView; }
        set
        {
            rangeOfView = value;
        }
    }
    public float farAway = 4.0f;

    public float FarAway
    {
        get { return farAway; }
        set
        {

            farAway = value;
        }
    }

    [HideInInspector]
    public float originalAngleOfView;

    public float angleOfView = 30; // +/-

    public float AngleOfView
    {
        get { return angleOfView * Mathf.Deg2Rad; }
        
    }

    public float Speed
    {
        get { return speed; }
        set
        {

            speed = value;
        }
    }




    public Transform startingPoint;

    Transform whereIam;

    public Transform WhereIam
    {
        set
        {
            whereIam = value;
        }
        get { return whereIam; }

    }

    public void SetTransition(Transition t) { fsm.PerformTransition(t); }

    public void Start()
    {
        exitPath = new ArrayList();
        statesToWarnAbout = new ArrayList();

        foreach (Transform t in ExitPath)
        {
            exitPath.Add(t);
        }
        PlayerScript = (MovePacman)FindObjectOfType(typeof(MovePacman));
        gameObject.transform.position = startingPoint.position;

        

        currentPath = myPath;

        MakeFSM();



        originalAngleOfView = AngleOfView;
        originalRangeOfView = RangeOfView;
              

        showRange = true;
    }

    void OnDrawGizmos()
    {

        if (currentPath == null)
            return;
        if (currentPath.Count > 0)
        {
            for (int index = 0; index < currentPath.Count - 1; index++)
            {
                if (currentPath[index] is WaypointHandler)
                    Debug.DrawLine(((WaypointHandler)currentPath[index]).position, ((WaypointHandler)currentPath[index + 1]).position, Color.green);
                else if (currentPath[index] is Transform)
                    Debug.DrawLine(((Transform)currentPath[index]).position, ((Transform)currentPath[index + 1]).position, Color.green);


            }
        }

        if (showRange)
        {
            Vector3 localForward = GetComponent<Rigidbody2D>().velocity.normalized;

            Vector3 perpendicular = Vector3.Cross(Vector3.forward, localForward).normalized;

            //Debug.DrawLine(transform.position, localForward + transform.position, Color.red);
            //Debug.DrawLine(transform.position, perpendicular + transform.position, Color.yellow);
            Color lineColor = Color.blue;
            if(estouVendoPacman()
                &&
                getDistanceToPacman()<=RangeOfView)
            {
                lineColor = Color.red;
            }


            Debug.DrawLine(transform.position, getLeftRange(), lineColor);
            Debug.DrawLine(transform.position, getRightRange(), lineColor);
        }





    }

    public Vector2 getForward()
    {
        return GetComponent<Rigidbody2D>().velocity.normalized;
    }

    public Vector2 getLeftRange()
    {

        Vector3 leftView = new Vector2(getForward().x * Mathf.Cos(-AngleOfView) - getForward().y * Mathf.Sin(-AngleOfView),
                                           getForward().y * Mathf.Cos(-AngleOfView) + getForward().x * Mathf.Sin(-AngleOfView));
        leftView = leftView * RangeOfView + transform.position;

        return leftView;
    }

    public Vector2 getRightRange()
    {

        Vector3 rightView = new Vector2(getForward().x * Mathf.Cos(AngleOfView) - getForward().y * Mathf.Sin(AngleOfView),
                                            getForward().y * Mathf.Cos(AngleOfView) + getForward().x * Mathf.Sin(AngleOfView));
        rightView = rightView * RangeOfView + transform.position;

        return rightView;
    }

    public Vector2 getDirectionToPacman()
    {
        Vector2 pacmanIs = new Vector2(player.transform.position.x, player.transform.position.y);
        Vector2 ghostIs = new Vector2(transform.position.x, transform.position.y);

        Vector2 dirToPacman = pacmanIs - ghostIs;

        return dirToPacman;

    }

    public float getDistanceToPacman()
    {
        float distanceToPacman = getDirectionToPacman().magnitude;
        return distanceToPacman;
    }

    public bool estouVendoPacman()
    {


        bool isInView = (Vector2.Angle(getForward(), getDirectionToPacman()) * Mathf.Deg2Rad >= -AngleOfView &&
                        Vector2.Angle(getForward(), getDirectionToPacman()) * Mathf.Deg2Rad <= AngleOfView);


        return isInView;
    }


    public void FixedUpdate()
    {
        fsm.CurrentState.Reason(player, gameObject);
        fsm.CurrentState.Act(player, gameObject);

        currentPath = ((GhostFSMState)fsm.CurrentState).Path;

        newDir = gameObject.GetComponent<Rigidbody2D>().velocity;

        //Cuida da mudança de animação

        myAnim.SetInteger("y", (int)newDir.y);
        myAnim.SetInteger("x", (int)newDir.x);


    }



    // The NPC has two states: FollowPath and ChasePlayer
    // If it's on the first state and SawPlayer transition is fired, it changes to ChasePlayer
    // If it's on ChasePlayerState and LostPlayer transition is fired, it returns to FollowPath
    private void MakeFSM()
    {
        InitialPathState initialPath = new InitialPathState(exitPath);
        initialPath.AddTransition(Transition.StartNormalGame, StateID.FollowingPath);
        statesToWarnAbout.Add(initialPath);

        FollowPathState follow = new FollowPathState(WaypointsOfInterest, HasMemory);
        follow.AddTransition(Transition.SawPlayer, StateID.GhostChasing);

        statesToWarnAbout.Add(follow);

        ChasePlayerState chase = new ChasePlayerState();
        chase.AddTransition(Transition.LostPlayer, StateID.FollowingPath);

        statesToWarnAbout.Add(chase);

        fsm = new FSMSystem();
        fsm.AddState(initialPath);
        fsm.AddState(follow);
        fsm.AddState(chase);
    }
}

public class InitialPathState : GhostFSMState
{
    public InitialPathState(ArrayList wp)
    {
        waypoints = wp;
        currentWayPoint = 0;
        stateID = StateID.InitialPath;
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        //Muda de estado quando os passos iniciais acabam
        if (currentWayPoint == waypoints.Count)
            npc.GetComponent<Ghost>().SetTransition(Transition.StartNormalGame);
    }

    public override void Act(GameObject player, GameObject npc)
    {
        // Follow the initial path of waypoints


        // Find the direction of the current way point 
        Vector3 vel = npc.GetComponent<Rigidbody2D>().velocity;
        Vector3 moveDir = ((Transform)waypoints[currentWayPoint]).position - npc.transform.position;

        //salva a informação de onde o Fantasma esta
        npc.GetComponent<Ghost>().WhereIam = (Transform)waypoints[currentWayPoint];

        if (moveDir.magnitude < .1)
        {
            currentWayPoint++;
        }
        else
        {
            vel = moveDir.normalized * npc.GetComponent<Ghost>().Speed;

        }

        // Apply the Velocity
        npc.GetComponent<Rigidbody2D>().velocity = vel;
    }

} // INITIAL STEPS

public class FollowPathState: GhostFSMState
{
    private int currentWayPoint;
    int currentPointOfInterest;

    ArrayList ghostWayPointsOfInterest;

    bool hasMemory = true;
    ArrayList myLastPath;
    int myLastWaypoint;


    public FollowPathState(Transform[] WaypointsOfInterest, bool HasMemory)
    {
        myLastPath = new ArrayList();
        ghostWayPointsOfInterest = new ArrayList();
        foreach (Transform t in WaypointsOfInterest)
        {
            ghostWayPointsOfInterest.Add(t);

        }

        hasMemory = HasMemory;


        currentWayPoint = 0;
        stateID = StateID.FollowingPath;

        waypoints = new ArrayList();

    }



    public override void Reason(GameObject player, GameObject npc)
    {
        // If the Player passes less than 15 meters away in front of the NPC
        if (!npc.GetComponent<Ghost>().sawPacman)
            npc.GetComponent<Ghost>().sawPacman = npc.GetComponent<Ghost>().estouVendoPacman()
                && npc.GetComponent<Ghost>().getDistanceToPacman() <= npc.GetComponent<Ghost>().RangeOfView;

        if (npc.GetComponent<Ghost>().sawPacman
            &&
            npc.GetComponent<Ghost>().canChasePacman //Se já viu o pacman entao
            )
        {
            Debug.Log("vi o pacman e estou perseguindo");
            //Guarda para memoria
            myLastPath.Clear();
            foreach (WaypointHandler t in waypoints)
                myLastPath.Add(t);
            myLastWaypoint = currentWayPoint;
            currentWayPoint = 0;
            waypoints.Clear();
            npc.GetComponent<Ghost>().sawPacman = false;

            //reduz angulo de visao para compensar o faraway
            //npc.GetComponent<Ghost>().AngleOfView = npc.GetComponent<Ghost>().originalAngleOfView / 3;
            npc.GetComponent<Ghost>().RangeOfView = npc.GetComponent<Ghost>().FarAway;
            npc.GetComponent<Ghost>().SetTransition(Transition.SawPlayer);
        }
    }

    ArrayList UsaEstrategia(WaypointHandler start, WaypointHandler goal, GameObject npc)
    {
        switch(npc.GetComponent<Ghost>().minhaEstrategia)
        {
            case ESTRATEGIA.MENOR_CAMINHO:{
                return CriaCaminho.menorCaminhoEntre(start,goal);
            }

            case ESTRATEGIA.WANDER_BETWEEN:
                {
                    return CriaCaminho.wanderBetween(start, goal);
                }

            case ESTRATEGIA.VAI_PARA_PLAYER:
                {
                    return CriaCaminho.wanderBetween(start, npc.GetComponent<Ghost>().player.GetComponent<MovePacman>().WhereIAm);
                }


            default:
                return CriaCaminho.menorCaminhoEntre(start,goal);
        }


    }

    public override void Act(GameObject player, GameObject npc)
    {
        //cria o caminho baseado nos pontos de interesses do Ghost

        if (waypoints.Count == 0)
        {

            if (hasMemory
                && myLastPath.Count > 0
                &&
                npc.GetComponent<Ghost>().WhereIam.name != ((WaypointHandler)myLastPath[myLastWaypoint]).name)//verifica se já não está onde deveria
            {
                waypoints = UsaEstrategia(npc.GetComponent<Ghost>().WhereIam.GetComponent<WaypointHandler>(), (WaypointHandler)myLastPath[myLastWaypoint], npc);
                for (int i = myLastWaypoint; i < myLastPath.Count; i++)
                {
                    if (!waypoints.Contains(myLastPath[myLastWaypoint]))
                        waypoints.Add(myLastPath[myLastWaypoint]);                    
                }
                

                

            }
            else
            {
                currentPointOfInterest = 0;
                waypoints = UsaEstrategia(npc.GetComponent<Ghost>().WhereIam.GetComponent<WaypointHandler>(), ((Transform)ghostWayPointsOfInterest[currentPointOfInterest]).GetComponent<WaypointHandler>(), npc);
                myLastPath = waypoints;
                currentPointOfInterest++;
                
            }

            currentWayPoint = 0;
            
        }



        // Follow the path of waypoints
        // Find the direction of the current way point 
        Vector3 vel = npc.GetComponent<Rigidbody2D>().velocity;        

        Vector3 moveDir = ((WaypointHandler)waypoints[currentWayPoint]).position - npc.transform.position;
        //salva a informação de onde o Fantasma esta
        npc.GetComponent<Ghost>().WhereIam = ((WaypointHandler)waypoints[currentWayPoint]).transform;

        if (moveDir.magnitude < .1)
        {
            currentWayPoint++;
            myLastWaypoint = currentWayPoint;

            if (currentWayPoint >= waypoints.Count)
            {
                if (currentPointOfInterest >= ghostWayPointsOfInterest.Count)
                    currentPointOfInterest = 0;
                waypoints = UsaEstrategia((WaypointHandler)waypoints[waypoints.Count - 1], ((Transform)ghostWayPointsOfInterest[currentPointOfInterest]).GetComponent<WaypointHandler>(), npc);
                currentWayPoint = 0;
                currentPointOfInterest++;

            }
            npc.GetComponent<Ghost>().canChasePacman = true;
        }
        else
        {
            vel = moveDir.normalized * npc.GetComponent<Ghost>().Speed;
            npc.GetComponent<Ghost>().canChasePacman = false;
        }

        // Apply the Velocity
        npc.GetComponent<Rigidbody2D>().velocity = vel;
        
    }

} // FollowPathState

public class ChasePlayerState : GhostFSMState
{
    bool stillSeeingPacman = false;

    public ChasePlayerState()
    {
        stateID = StateID.GhostChasing;
        waypoints = new ArrayList();
    }


    public override void Reason(GameObject player, GameObject npc)
    {
        //verifica se perdeu somente no final do percurso. Se o Pacman ainda estiver em range, outro caminho sera construido
        if (waypoints.Count == 0 || npc.GetComponent<Ghost>().WhereIam == (WaypointHandler)waypoints[waypoints.Count - 1])
        {
            if (npc.GetComponent<Ghost>().getDistanceToPacman() >= npc.GetComponent<Ghost>().RangeOfView) //se estiver alem do campo de visao
                
            {              

                Debug.Log("Perdi o Pacman de vista em " + npc.GetComponent<Ghost>().WhereIam.name);
                npc.GetComponent<Ghost>().canChasePacman = false;               
                npc.GetComponent<Ghost>().RangeOfView = npc.GetComponent<Ghost>().originalRangeOfView;
                npc.GetComponent<Ghost>().SetTransition(Transition.LostPlayer);
            }
        }
    }

    public override void Act(GameObject player, GameObject npc)
    {
        // Follow the path of waypoints
        // Find the direction of the player
        if (waypoints.Count == 0)
        {
            //Debug.Log("Quero ir de " + npc.GetComponent<Ghost>().WhereIam.name + " para " + player.GetComponent<MovePacman>().WhereIAm.name);
            waypoints = CriaCaminho.menorCaminhoEntre(npc.GetComponent<Ghost>().WhereIam, player.GetComponent<MovePacman>().WhereIAm);

            
        }

        Vector3 vel = npc.GetComponent<Rigidbody2D>().velocity;
        Vector3 moveDir = ((WaypointHandler)waypoints[currentWayPoint]).position - npc.transform.position;
        //salva a informação de onde o Fantasma esta
        npc.GetComponent<Ghost>().WhereIam = ((WaypointHandler)waypoints[currentWayPoint]).transform;

        if (moveDir.magnitude < .1)
        {
            currentWayPoint++;
            if (currentWayPoint >= waypoints.Count)
            {
                waypoints.Clear();
                currentWayPoint = 0;
            }
        }



        vel = moveDir.normalized * npc.GetComponent<Ghost>().Speed;

        //// Apply the new Velocity
        npc.GetComponent<Rigidbody2D>().velocity = vel;
    }

} // ChasePlayerState