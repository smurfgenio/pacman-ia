﻿using UnityEngine;
using System.Collections;

public class GridManager : MonoBehaviour {
    
    public WaypointHandler[] Waypoints;

   
    private static GridManager s_Instance = null;
    public static GridManager instance
    {
        get
        {
            if (s_Instance == null)
            {
                s_Instance = FindObjectOfType(typeof(GridManager)) as GridManager;
                if (s_Instance == null)
                    Debug.Log("Could not locate a GridManager " +
                    "object. \n You have to have exactly " +
                    "one GridManager in the scene.");
            }
            return s_Instance;
        }
    }

    void Awake()
    {
        Waypoints = FindObjectsOfType<WaypointHandler>();        
    }

   public static WaypointHandler getWayPointByName(string wayPointName)
   {
        for (int i = 0; i < GridManager.instance.Waypoints.Length; i++)
        {
            if (GridManager.instance.Waypoints[i].name == wayPointName)
                return (WaypointHandler)GridManager.instance.Waypoints[i];
        }

            
       return null;
    }

    public static WaypointHandler getRandomWaypoint()
   {
       Random.seed = System.DateTime.Now.Second + System.DateTime.Now.Day + System.DateTime.Now.Year - System.DateTime.Now.Month;
       int index = Random.Range(0, GridManager.instance.Waypoints.Length);

       return GridManager.instance.Waypoints[index];
   }

}
