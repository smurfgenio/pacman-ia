﻿using UnityEngine;
using System.Collections;

public enum ESTRATEGIA
{
    MENOR_CAMINHO,
    WANDER_BETWEEN,
    VAI_PARA_PLAYER
};

public abstract class GhostFSMState : FSMState {

    //Adiciona caracteristicas que sao proprias do fantasma, para manter a FSMstate limpa
    protected int currentWayPoint;
    protected ArrayList waypoints;
    
    public ArrayList Path
    {
        get
        {
            return waypoints;
        }
    }
}
