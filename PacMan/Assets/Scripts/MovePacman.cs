﻿using UnityEngine;
using System.Collections;

public class MovePacman : MonoBehaviour
{

    public Transform startPoint;
    public Animator myAnim;
    Transform whereIam;
    Transform nextWayPoint;

    bool iMAlive = true;

    public Transform NextWayPoint
    {
        get { return nextWayPoint; }
        set { nextWayPoint = value; }
    }
    Vector2 movingTo; //Direção do movimento, verificar proximo way point da direção

    public Vector2 IsMovingTo
    {
        get
        {
            return movingTo;
        }
    }


    bool onlyHorizontal = false;
    bool onlyVertical = false;

    bool moveToPortal = false;


    public float speed = 5.0f;

    public WaypointHandler WhereIAm
    {
        get
        {
            return whereIam.GetComponent<WaypointHandler>();
        }
    }

    float timeToReset = 0.0f;



    // Use this for initialization
    void Start()
    {
        gameObject.transform.position = startPoint.transform.position;
        movingTo = Vector2.zero;
        if (startPoint != null)
            whereIam = startPoint;
        NextWayPoint = null;

    }

    // Update is called once per frame
    void Update()
    {
        WaypointHandler aux = whereIam.GetComponent<WaypointHandler>();

        if (iMAlive)
        {
            if (!moveToPortal)
            {
                if (Input.GetAxis("Horizontal") > 0)
                    movingTo = Vector2.right;

                if (Input.GetAxis("Horizontal") < 0)
                    movingTo = -Vector2.right;

                if (Input.GetAxis("Vertical") > 0)
                    movingTo = Vector2.up;

                if (Input.GetAxis("Vertical") < 0)
                    movingTo = -Vector2.up;

            }
            if (movingTo == Vector2.right && !onlyVertical)
            {

                if (aux.nextRight != null)
                {
                    onlyHorizontal = true;
                    NextWayPoint = aux.nextRight;
                    myAnim.SetInteger("direcao", 0);
                }
            }


            if (movingTo == -Vector2.right && !onlyVertical)
            {
                if (aux.nextLeft != null)
                {
                    onlyHorizontal = true;
                    NextWayPoint = aux.nextLeft;

                    myAnim.SetInteger("direcao", 1);
                }
            }



            if (movingTo == Vector2.up && !onlyHorizontal)
            {
                if (aux.nextUP != null)
                {
                    onlyVertical = true;
                    NextWayPoint = aux.nextUP;
                    myAnim.SetInteger("direcao", 2);
                }
            }

            if (movingTo == -Vector2.up && !onlyHorizontal)
            {
                if (aux.nextDown != null)
                {
                    onlyVertical = true;
                    NextWayPoint = aux.nextDown;
                    myAnim.SetInteger("direcao", 3);
                }
            }


            if (aux.isPortalto != null)
                if (aux.isPortalto == NextWayPoint)
                {
                    moveToPortal = true;

                }

        }
        else
        {
            //PACMAN is dead
            
            //Espera animação terminar e reseta a cena  
            Debug.Log(timeToReset);
            if (Time.time >= timeToReset)
                Application.LoadLevel(Application.loadedLevelName);
        }

    }

    void FixedUpdate()
    {
        Vector3 vel = gameObject.GetComponent<Rigidbody2D>().velocity;
        Vector3 moveDir;

        if (iMAlive)
        {
            if (!moveToPortal)
            {


                if (NextWayPoint != null)
                {
                    moveDir = NextWayPoint.position - gameObject.transform.position;

                    if (moveDir.magnitude < 0.1f)
                    {
                        whereIam = NextWayPoint;
                        gameObject.transform.position = whereIam.position;

                        onlyHorizontal = false;
                        onlyVertical = false;
                        moveDir = Vector3.zero;

                    }
                    else
                    {

                        vel = moveDir.normalized * speed;


                    }

                    // Apply the Velocity
                    gameObject.GetComponent<Rigidbody2D>().velocity = vel;
                }
            }

            if (moveToPortal)
            {
                whereIam = NextWayPoint;
                gameObject.transform.position = NextWayPoint.position;
                gameObject.GetComponent<Rigidbody2D>().velocity = -gameObject.GetComponent<Rigidbody2D>().velocity;


                onlyHorizontal = false;
                onlyVertical = false;
                moveDir = Vector3.zero;
                moveToPortal = false;
            }
        }
        else
        {
            //PACMAN is dead
            gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "ghost")
        {
            if(iMAlive)
                timeToReset = Time.time + myAnim.GetCurrentAnimatorStateInfo(0).length + 1.0f;
            myAnim.SetBool("dead", true);
            iMAlive = false;
            
        }
    }
}
