﻿using UnityEngine;
using System.Collections;

  [System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class OpenMapMovePacman : MonoBehaviour {

    Vector3 moveDir;
    public float speed = 10.0f;
    public Boundary boundary;
    public float tilt;

   
  

	// Use this for initialization
	void Start () {
        
	
	}
	
	// Update is called once per frame
	void Update () {
       
	
	}

    void FixedUpdate()
    {

        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");

        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
        GetComponent<Rigidbody>().velocity = movement * speed;

        GetComponent<Rigidbody>().position = new Vector3 
        (
            Mathf.Clamp (GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax), 
            transform.position.y, 
            Mathf.Clamp (GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)
        );

        GetComponent<Rigidbody>().rotation = Quaternion.Euler (0.0f, 0.0f, GetComponent<Rigidbody>().velocity.x * -tilt);
    }

    
}
