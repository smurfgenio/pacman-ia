﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class OpenMapMoveGhost : MonoBehaviour
{

    private Transform startPos, endPos;

    public GameObject pacman;

    public Node startNode { get; set; }
    public Node goalNode { get; set; }
    public ArrayList pathArray;

    [HideInInspector]
    public float elapsedTime = 0.0f;
    //Interval time between pathfinding

    
    public float intervalTime = 1.0f;
    public float Speed = 5.0f;
    public float MyForce = 100.0f;

    public bool showPath = true;

    //Sensors
    public float rangeOfView = 2.5f;
    public float farAway = 4.0f;
    public float angleOfView = 30;
    public float AngleOfView
    {
        get { return angleOfView * Mathf.Deg2Rad; }

    }

    public bool showRange = true;


    ArrayList currentPath;

    private FSMSystem fsm;
    Vector3 newDir;

    public void SetTransition(Transition t) { fsm.PerformTransition(t); }

    void Start()
    {

        pathArray = new ArrayList();
        MakeFSM();
    }
    void Update()
    {

    }

    public ArrayList FindPath(Transform goal)
    {
        startPos = gameObject.transform;
        endPos = goal;
        startNode = new Node(OpenMapGridManager.instance.GetGridCellCenter(
        OpenMapGridManager.instance.GetGridIndex(startPos.position)));
        goalNode = new Node(OpenMapGridManager.instance.GetGridCellCenter(
        OpenMapGridManager.instance.GetGridIndex(endPos.position)));

        return AStar.FindPath(startNode, goalNode);
    }

    void OnDrawGizmos()
    {
        if (currentPath == null)
            return;
        if (currentPath.Count > 0 && showPath)
        {
            int index = 0;
            foreach (Node node in currentPath)
            {

                if (index < currentPath.Count - 1)
                {
                    Node nextNode = (Node)currentPath[index + 1];
                    Debug.DrawLine(node.position, nextNode.position, Color.green);
                    index++;
                }
            }
        }

        if (showRange)
        {
            Vector3 localForward = GetComponent<Rigidbody>().velocity.normalized;

            Vector3 perpendicular = Vector3.Cross(Vector3.forward, localForward).normalized;

            //Debug.DrawLine(transform.position, localForward + transform.position, Color.red);
            //Debug.DrawLine(transform.position, perpendicular + transform.position, Color.yellow);
            Color lineColor = Color.blue;
            if (estouVendoPacman()
                &&
                getDistanceToPacman() <= rangeOfView)
            {
                lineColor = Color.red;
            }


            Debug.DrawLine(transform.position, getLeftRange(), lineColor);
            Debug.DrawLine(transform.position, getRightRange(), lineColor);
        }
    }

    public Vector3 getForward()
    {
        Vector3 returnVec = GetComponent<Rigidbody>().velocity.normalized;

        if (returnVec.magnitude == 0)
            returnVec = Vector3.forward;

        return returnVec;
    }

    public Vector3 getLeftRange()
    {

        Vector3 leftView = new Vector3(getForward().x * Mathf.Cos(-AngleOfView) - getForward().z * Mathf.Sin(-AngleOfView),0,
                                           getForward().z * Mathf.Cos(-AngleOfView) + getForward().x * Mathf.Sin(-AngleOfView));
        leftView = leftView * rangeOfView + transform.position;

        return leftView;
    }

    public Vector3 getRightRange()
    {

        Vector3 rightView = new Vector3(getForward().x * Mathf.Cos(AngleOfView) - getForward().z * Mathf.Sin(AngleOfView),0,
                                            getForward().z * Mathf.Cos(AngleOfView) + getForward().x * Mathf.Sin(AngleOfView));
        rightView = rightView * rangeOfView + transform.position;

        return rightView;
    }

    public Vector3 getDirectionToPacman()
    {
        Vector3 pacmanIs = pacman.transform.position;
        Vector3 ghostIs = transform.position;

        Vector3 dirToPacman = pacmanIs - ghostIs;

        return dirToPacman;

    }

    public float getDistanceToPacman()
    {
        float distanceToPacman = getDirectionToPacman().magnitude;
        return distanceToPacman;
    }

    public bool estouVendoPacman()
    {


        bool isInView = (Vector2.Angle(getForward(), getDirectionToPacman()) * Mathf.Deg2Rad >= -AngleOfView &&
                        Vector2.Angle(getForward(), getDirectionToPacman()) * Mathf.Deg2Rad <= AngleOfView);


        return isInView;
    }


    //Maquina de estados
    public void FixedUpdate()
    {
        fsm.CurrentState.Reason(pacman, gameObject);
        fsm.CurrentState.Act(pacman, gameObject);


        currentPath = ((GhostFSMState)fsm.CurrentState).Path;

        newDir = gameObject.GetComponent<Rigidbody>().velocity;


    }

    private void MakeFSM()
    {

        FollowPathState3D follow = new FollowPathState3D();
        follow.AddTransition(Transition.SawPlayer, StateID.GhostChasing);

        ChasePlayerState3D chase = new ChasePlayerState3D();
        chase.AddTransition(Transition.LostPlayer, StateID.FollowingPath);


        fsm = new FSMSystem();
        fsm.AddState(follow);
        fsm.AddState(chase);
    }
}

public class FollowPathState3D : GhostFSMState
{

    public FollowPathState3D()
    {
        waypoints = new ArrayList();
        currentWayPoint = 0;
        stateID = StateID.FollowingPath;
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        npc.GetComponent<OpenMapMoveGhost>().SetTransition(Transition.SawPlayer);
    }

    public override void Act(GameObject player, GameObject npc)
    {
        if(waypoints.Count==0
            || currentWayPoint >= waypoints.Count)
        {

        }
        
    }
}


public class ChasePlayerState3D : GhostFSMState
{
    public ChasePlayerState3D()
    {
        waypoints = new ArrayList();
        currentWayPoint = 0;
        stateID = StateID.GhostChasing;
    }

    public override void Reason(GameObject player, GameObject npc)
    {
    }

    public override void Act(GameObject player, GameObject npc)
    {

        npc.GetComponent<OpenMapMoveGhost>().elapsedTime += Time.deltaTime;

        if (waypoints.Count == 0
            ||
            npc.GetComponent<OpenMapMoveGhost>().elapsedTime >= npc.GetComponent<OpenMapMoveGhost>().intervalTime)
        {
            npc.GetComponent<OpenMapMoveGhost>().elapsedTime = 0.0f;
            waypoints = npc.GetComponent<OpenMapMoveGhost>().FindPath(player.transform);
            currentWayPoint = 0;
        }

        Vector3 vel = npc.GetComponent<Rigidbody>().velocity;
        Vector3 moveDir = ((Node)waypoints[currentWayPoint]).position - npc.transform.position;

        Debug.Log(moveDir.magnitude);

        if (moveDir.magnitude < .5)
        {
            currentWayPoint++;
            if (currentWayPoint >= waypoints.Count)
            {
                //Refaz o caminho até o pacman
                //Melhorar isso pois essa é a logica do CHASE e nao do FOLLOW.
                //Mantendo assim para testar.
                waypoints = npc.GetComponent<OpenMapMoveGhost>().FindPath(player.transform);
                currentWayPoint = 0;
            }
        }
        else
        {
            vel = moveDir.normalized * npc.GetComponent<OpenMapMoveGhost>().Speed;
        }

       

        npc.GetComponent<Rigidbody>().velocity = vel;
        npc.GetComponent<Rigidbody>().AddRelativeForce(vel * npc.GetComponent<OpenMapMoveGhost>().MyForce);

        npc.GetComponent<Rigidbody>().position = new Vector3
        (
            Mathf.Clamp(npc.GetComponent<Rigidbody>().position.x, 0, 20),
            npc.transform.position.y,
            Mathf.Clamp(npc.GetComponent<Rigidbody>().position.z, 0, 20)
        );
    }
}



