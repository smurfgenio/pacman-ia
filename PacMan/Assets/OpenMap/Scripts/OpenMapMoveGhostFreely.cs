﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class OpenMapMoveGhostFreely : MonoBehaviour
{

    private Transform startPos, endPos;

    public GameObject pacman;

    public Node startNode { get; set; }
    public Node goalNode { get; set; }
    public ArrayList pathArray;

    [HideInInspector]
    public float elapsedTime = 0.0f;
    //Interval time between pathfinding


    public float intervalTime = 1.0f;
    public float Speed = 5.0f;
    public float MyForce = 100.0f;

    public bool showPath = true;

    //Sensors
    public float rangeOfView = 2.5f;
    public float farAway = 4.0f;
    public float angleOfView = 30;
    public float AngleOfView
    {
        get { return angleOfView * Mathf.Deg2Rad; }

    }

    public bool showRange = true;


    ArrayList currentPath;

    private FSMSystem fsm;
    Vector3 newDir;

    public void SetTransition(Transition t) { fsm.PerformTransition(t); }

    void Start()
    {

        pathArray = new ArrayList();
        MakeFSM();
    }
    void Update()
    {

    }

    public ArrayList FindPath(Transform goal)
    {
        startPos = gameObject.transform;
        endPos = goal;
        startNode = new Node(OpenMapGridManager.instance.GetGridCellCenter(
        OpenMapGridManager.instance.GetGridIndex(startPos.position)));
        goalNode = new Node(OpenMapGridManager.instance.GetGridCellCenter(
        OpenMapGridManager.instance.GetGridIndex(endPos.position)));

        return AStar.FindPath(startNode, goalNode);
    }

    void OnDrawGizmos()
    {


        if (showRange)
        {
            Vector3 localForward = GetComponent<Rigidbody>().velocity.normalized;

            Vector3 perpendicular = Vector3.Cross(Vector3.forward, localForward).normalized;

            Debug.DrawLine(transform.position, localForward + transform.position, Color.red);
            //Debug.DrawLine(transform.position, perpendicular + transform.position, Color.yellow);
            Color lineColor = Color.blue;
            if (estouVendoPacman()
                &&
                getDistanceToPacman() <= rangeOfView)
            {
                lineColor = Color.red;
            }


            Debug.DrawLine(transform.position, getLeftRange(), lineColor);
            Debug.DrawLine(transform.position, getRightRange(), lineColor);
        }
    }

    public Vector3 getForward()
    {
        Vector3 returnVec = GetComponent<Rigidbody>().velocity.normalized;

        if (returnVec.magnitude == 0)
            returnVec = Vector3.forward;

        return returnVec;
    }

    public Vector3 getLeftRange()
    {

        Vector3 leftView = new Vector3(getForward().x * Mathf.Cos(-AngleOfView) - getForward().z * Mathf.Sin(-AngleOfView), 0,
                                           getForward().z * Mathf.Cos(-AngleOfView) + getForward().x * Mathf.Sin(-AngleOfView));
        leftView = leftView * rangeOfView + transform.position;

        return leftView;
    }

    public Vector3 getRightRange()
    {

        Vector3 rightView = new Vector3(getForward().x * Mathf.Cos(AngleOfView) - getForward().z * Mathf.Sin(AngleOfView), 0,
                                            getForward().z * Mathf.Cos(AngleOfView) + getForward().x * Mathf.Sin(AngleOfView));
        rightView = rightView * rangeOfView + transform.position;

        return rightView;
    }

    public Vector3 getDirectionToPacman()
    {
        Vector3 pacmanIs = pacman.transform.position;
        Vector3 ghostIs = transform.position;

        Vector3 dirToPacman = pacmanIs - ghostIs;

        return dirToPacman;

    }

    public float getDistanceToPacman()
    {
        float distanceToPacman = getDirectionToPacman().magnitude;
        return distanceToPacman;
    }

    public bool estouVendoPacman()
    {


        bool isInView = (Vector2.Angle(getForward(), getDirectionToPacman()) * Mathf.Deg2Rad >= -AngleOfView &&
                        Vector2.Angle(getForward(), getDirectionToPacman()) * Mathf.Deg2Rad <= AngleOfView);


        return isInView;
    }


    //Maquina de estados
    public void FixedUpdate()
    {
        fsm.CurrentState.Reason(pacman, gameObject);
        fsm.CurrentState.Act(pacman, gameObject);


        currentPath = ((GhostFSMState)fsm.CurrentState).Path;

        newDir = gameObject.GetComponent<Rigidbody>().velocity;


    }

    private void MakeFSM()
    {

        Wander3D follow = new Wander3D();
        follow.AddTransition(Transition.SawPlayer, StateID.GhostChasing);

        PursuitState chase = new PursuitState();
        chase.AddTransition(Transition.LostPlayer, StateID.FollowingPath);


        fsm = new FSMSystem();
        //fsm.AddState(follow);
        fsm.AddState(chase);
    }
}

public class Wander3D : GhostFSMState
{

    public Wander3D()
    {
        waypoints = new ArrayList();
        currentWayPoint = 0;
        stateID = StateID.FollowingPath;
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        npc.GetComponent<OpenMapMoveGhostFreely>().SetTransition(Transition.SawPlayer);
    }

    public override void Act(GameObject player, GameObject npc)
    {
        if (waypoints.Count == 0
            || currentWayPoint >= waypoints.Count)
        {

        }

    }
}


public class PursuitState : GhostFSMState
{
    public PursuitState()
    {
        stateID = StateID.GhostChasing;
    }

    public override void Reason(GameObject player, GameObject npc)
    {
    }

    public void AvoidObstacles(ref Vector3 dir, GameObject npc, float minimumDistToAvoid, float force)
    {
        RaycastHit hit;
        //Only detect layer 9 (Obstacles)
        int layerMask = 1 << 9;

        //Check that the vehicle hit with the obstacles within
        //it's minimum distance to avoid
        if (Physics.Raycast(npc.transform.position, npc.transform.forward, out hit, minimumDistToAvoid, layerMask))
        {
            //Get the normal of the hit point to calculate the
            //new direction
            Vector3 hitNormal = hit.normal;
            hitNormal.y = 0.0f; //Don't want to move in Y-Space

            //Get the new directional vector by adding force to
            //vehicle's current forward vector
            dir = npc.transform.forward + hitNormal * force;
        }
    }


    public override void Act(GameObject player, GameObject npc)
    {

        npc.GetComponent<OpenMapMoveGhostFreely>().elapsedTime += Time.deltaTime;



        Vector3 vel = npc.GetComponent<Rigidbody>().velocity;
        Vector3 moveDir = player.transform.position - npc.transform.position;

        AvoidObstacles(ref moveDir, npc, 2.0f, npc.GetComponent<OpenMapMoveGhostFreely>().MyForce);

        
        Debug.Log(moveDir.magnitude);

        vel = moveDir.normalized * npc.GetComponent<OpenMapMoveGhostFreely>().Speed;


        npc.GetComponent<Rigidbody>().velocity = vel;
        npc.GetComponent<Rigidbody>().AddRelativeForce(vel * npc.GetComponent<OpenMapMoveGhostFreely>().MyForce);

        npc.GetComponent<Rigidbody>().position = new Vector3
        (
            Mathf.Clamp(npc.GetComponent<Rigidbody>().position.x, 0, 20),
            npc.transform.position.y,
            Mathf.Clamp(npc.GetComponent<Rigidbody>().position.z, 0, 20)
        );
    }
}
